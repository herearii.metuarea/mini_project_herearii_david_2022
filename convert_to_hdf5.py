# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 21:17:44 2022

@author: Metuarea Herearii
"""

#Convert tif to hdf5

import h5py
import os
from glob import glob
import numpy as np
import imageio

#path: location of aphids snap
#example : folder named convert
PATH = os.path.abspath('Desktop\\Process\\France')

#images: all images in tif
images = glob(os.path.join(PATH, "*.tif")) 

#for each images tif, we get images in hdf5

#example
#input snap-112.tif
#output snapx112.h5

#directory where snaps will be saved
SAVE_PATH = os.path.abspath('Desktop\\Process\\Raw Data')
for imgg in images:
    #number num = re.findall(r'\d+',imgg)[0] #snap number
    #num = os.path.join(SAVE_PATH,'X'+'_'.join(imgg.split('\\')[-1].split('.')[:-1]))
    num = 'X_'+'_'.join(imgg.split('\\')[-1].split('.')[:-1]) #name of image in hdf5
    with h5py.File(num+'.h5', 'w') as hf:           
        image = imageio.imread(imgg)
        Xset = hf.create_dataset(
            name='X_'+num,
            data=image,
            shape=image.shape, compression='gzip',dtype=np.uint8)
        hf.close()
        
#snapx in h5 are saved in same folder where snap in tif are saved