# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 22:02:40 2022

@author: Metuarea Herearii
"""

import os
import shutil
from glob import glob
import pandas as pd

###########
###########
#Train#####
###########
###########

#Path where output of training set is located
PATH_1 = os.path.abspath('Desktop\\Process\\Output Object classification\\train')

#list of all table of training set in csv
pred_label_path_1 = glob(os.path.join(PATH_1, "*.csv"))

#empty list where we will saved predicted class of all images
pred_table_path_1= []

#for each table of training set in csv (1), we convert in data frame (2) and we save only Predicted class column (3)
for tableau in pred_label_path_1: #(1)
    data=pd.read_csv(tableau) #(2)
    pred_table_path_1 = list(data['Predicted Class'])+pred_table_path_1 #(3)

###########
###########
#Test######
###########
###########

#Path where output of test set is located
PATH_2 = os.path.abspath('Desktop\\Process\\Output Object classification\\test')

#list of all table of test set in csv
pred_label_path_2 = glob(os.path.join(PATH_2, "*.csv"))

#empty list where we will saved predicted class of all images
pred_table_path_2= []

#for each table of test set in csv (1), we convert in data frame (2) and we save only Predicted class column (3)
for tableau in pred_label_path_2:
    data=pd.read_csv(tableau)
    pred_table_path_2 = list(data['Predicted Class'])+pred_table_path_2

###########
###########

#list concatenated of lists of predict class of images from test set and training set
pred_table = pred_table_path_1 + pred_table_path_2

###########
###########
#Plot######
###########
###########
from collections import Counter
import numpy as np

# Dictionnary of aggregated aphids categories
temp = dict(Counter(pred_table))

# Design bar plot
import matplotlib.pyplot as plt

height = list(temp.values())
lab = list(temp.keys())
y_pos = np.arange(len(lab))

# Create bars
plt.bar(y_pos, height)

# Create names on the x-axis
plt.xticks(y_pos, lab, rotation=90)

for i,v in enumerate(height):
    plt.text(x=i-.5 , y =v+1 , s=f"{v}" , fontdict=dict(fontsize=20))

# Show graphic
plt.show()
