# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 11:36:21 2022

@author: Metuarea Herearii
"""

import os

#Path where original images (.hdf5) are located
PATH = os.path.abspath('Desktop\\Process\\Raw Data')

import os
from glob import glob

#list of original images (.hdf5)
original = glob(os.path.join(PATH, "*.h5"))
            
#Split the list "original" intro test and train set
#Train set : 10% of images (17 images)
#Test set : 90% of images (162 images)
from sklearn.model_selection import train_test_split
train_data, test_data = train_test_split(original,train_size=0.1)

##############
##############
#Save images of train set and images of test set in respective folders
##############
##############
   
import shutil

#Path where folder of train set and folder of test set will be saved
SAVE_PATH = os.path.abspath('Desktop\\Process\\Train_test_dataset')

#Create new folder : test
os.makedirs(SAVE_PATH+'\\test')

#For each images in test set (1), we get name of snaps (2) and save images into test folder (3)
for file in test_data: #(1)
    snaps = file.split('\\')[-1] #(2)
    shutil.copy(file,SAVE_PATH+'\\test\\'+snaps) #(3)

#Create new folder : train
os.makedirs(SAVE_PATH+'\\train')

#For each images in train set (1), we get name of snaps (2) and save images into train folder (3)
for file in train_data: #(1)
    snaps = file.split('\\')[-1] #(2)
    shutil.copy(file,SAVE_PATH+'\\train\\'+snaps) #(3)
